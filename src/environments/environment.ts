// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBiQMk8cKFnNBacbcnRvKvjcMgqwpyOh7E",
    authDomain: "angular-material-55170.firebaseapp.com",
    databaseURL: "https://angular-material-55170.firebaseio.com",
    projectId: "angular-material-55170",
    storageBucket: "angular-material-55170.appspot.com",
    messagingSenderId: "34030181712"
  }
};
